function reduce(elements, cb, startingValue) {
  if (!Array.isArray(elements)) return null;

  for (let i = 0; i < elements.length; i++) {
    if (startingValue !== undefined) {
      startingValue = cb(startingValue, elements[i], i, elements);
    } else if (startingValue === undefined) {
      startingValue = elements[0];
    }
  }

  return startingValue;
}

module.exports = reduce;
