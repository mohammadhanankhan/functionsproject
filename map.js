function map(elements, cb) {
  if (!Array.isArray(elements)) return null;

  const mappedArr = [];

  for (let i = 0; i < elements.length; i++) {
    let value = cb(elements[i], i, elements);
    mappedArr.push(value);
  }

  return mappedArr;
}

module.exports = map;
