const filter = require('../filter.js');

const items = [1, 2, 3, 4, 5, 5];

const cb = (item, i, arr) => {
  return item % 2 === 0;
};

const result = filter(items, cb);

console.log(result);
