const each = require('../each.js');

const items = [1, 2, 3, 4, 5, 5];

const cb = (item, i, arr) => {
  console.log(`Element at index ${i} is ${item}`);
};

const result = each(items, cb);

console.log(result);
