const map = require('../map.js');

const items = [1, 2, 3, 4, 5, 5];

const cb = (item, i, arr) => {
  return item * 10;
};
const result = map(items, cb);

console.log(result);
