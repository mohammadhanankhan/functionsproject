const reduce = require('../reduce.js');

const items = [1, 2, 3, 4, 5, 5];

const cb = (acc, cur, i, arr) => {
  return acc + cur;
};

const result = reduce(items, cb);

console.log(result);
