const find = require('../find.js');

const items = [1, 2, 3, 4, 5, 5];

const cb = (item, i, arr, searchValue) => {
  return item === searchValue;
};

const result = find(items, cb, 4);

console.log(result);
