function filter(elements, cb) {
  if (!Array.isArray(elements)) return null;

  const filterArr = [];

  for (let i = 0; i < elements.length; i++) {
    if (cb(elements[i], i, elements)) {
      filterArr.push(elements[i]);
    }
  }

  return filterArr;
}

module.exports = filter;
