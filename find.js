function find(elements, cb, searchValue) {
  if (!Array.isArray(elements)) return null;

  for (let i = 0; i < elements.length; i++) {
    if (cb(elements[i], i, elements, searchValue)) {
      return elements[i];
    }
  }
}

module.exports = find;
