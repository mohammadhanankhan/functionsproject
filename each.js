function each(elements, cb) {
  if (!Array.isArray(elements)) return null;

  for (let i = 0; i < elements.length; i++) {
    cb(elements[i], i, elements);
  }
}

module.exports = each;
