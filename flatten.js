const flatArr = [];

function flatten(elements) {
  for (let i = 0; i < elements.length; i++) {
    if (Array.isArray(elements[i])) {
      flatten(elements[i]);
    } else {
      flatArr.push(elements[i]);
    }
  }

  return flatArr;
}

module.exports = flatten;
